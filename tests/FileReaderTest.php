<?php

use DataMock\Helper\FileReader;

class FileReaderTest extends \PHPUnit\Framework\TestCase {

	public function testExceptionThrownWhenFileNotFound() {
		$this->expectException(\Exception::class);

		$fileReader = new FileReader("/path/that/is/invalid/");
	}

	public function testCanProcessAnEmptyCsvFile() {
		$fileReader = new FileReader(realpath(dirname(__FILE__) . "/test.csv"));
		$arData = $fileReader->parseFileAsCsv();

		$this->assertEmpty($arData);

		$fileReader->deleteFileInMemory();
	}

}

?>