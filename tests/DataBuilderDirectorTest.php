<?php

use DataMock\DataBuilderDirector;

use DataMock\Builder\IdBuilder;
use DataMock\Builder\Ipv4Builder;
use DataMock\Builder\UuidBuilder;
use DataMock\Builder\FirstNameBuilder;

use DataMock\Column\IdDataColumn;
use DataMock\Column\Ipv4DataColumn;
use DataMock\Column\UuidDataColumn;
use DataMock\Column\SingleWordDataColumn;

use DataMock\Settings\IdSettings;
use DataMock\Settings\UuidSettings;
use DataMock\Settings\BaseSettings;

class DataBuilderDirectorTest extends \PHPUnit\Framework\TestCase {
	private $director;
	private $settings;

	public function setUp() {
		$this->settings = new BaseSettings();

		$this->settings->setRowCount(10);
		$this->settings->setFieldName("test");

		$this->director = new DataBuilderDirector($this->settings);
	}

	public function testCanBuildId() {
		$settings = new IdSettings();
		$idBuilder = new IdBuilder();

		$settings->setRowCount(10);
		$settings->setFieldName("test");

		$this->director->setSettings($settings);
		$idColumn = $this->director->build($idBuilder);

		// Generic test for all DataColumn types
		$this->assertInstanceOf(IdDataColumn::class, $idColumn);
		$this->assertCount(10, $idColumn->getData());
		$this->assertEquals("test", $idColumn->getFieldName());

		// Test default settings
		$this->assertEquals(1, $idColumn->getData()[0]);
		$this->assertEquals(10, $idColumn->getData()[9]);

		$settings->setStartValue(33);

		$this->director->setSettings($settings);
		$idColumn = $this->director->build($idBuilder);

		// Test positive number starting value, other than default
		$this->assertEquals(33, $idColumn->getData()[0]);
		$this->assertEquals(42, $idColumn->getData()[9]);

		$settings->setStartValue(-3);

		$this->director->setSettings($settings);
		$idColumn = $this->director->build($idBuilder);

		// Test negative number starting value
		$this->assertEquals(-3, $idColumn->getData()[0]);
		$this->assertEquals(6, $idColumn->getData()[9]);
	}

	public function testCanBuildIpv4() {
		$ipv4Column = $this->director->build(new Ipv4Builder());

		$this->assertInstanceOf(Ipv4DataColumn::class, $ipv4Column);
		$this->assertCount(10, $ipv4Column->getData());
		$this->assertEquals("test", $ipv4Column->getFieldName());
	}

	public function testCanBuildUuid() {
		$settings = new UuidSettings();
		$uuidBuilder = new UuidBuilder();

		$settings->setRowCount(10);
		$settings->setFieldName("test");

		$this->director->setSettings($settings);
		$uuidColumn = $this->director->build($uuidBuilder);

		// Generic test for all DataColumn types
		$this->assertInstanceOf(UuidDataColumn::class, $uuidColumn);
		$this->assertCount(10, $uuidColumn->getData());
		$this->assertEquals("test", $uuidColumn->getFieldName());

		// Test default format --> XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		$this->assertEquals(32, strlen($uuidColumn->getData()[0]));

		$settings->setHyphensDesired(TRUE);

		$this->director->setSettings($settings);
		$uuidColumn = $this->director->build($uuidBuilder);

		// Test standard format --> XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
		$this->assertEquals(36, strlen($uuidColumn->getData()[0]));
		$this->assertEquals(5, count(explode("-", $uuidColumn->getData()[0])));

		$settings->setBracketsDesired(TRUE);
		$settings->setHyphensDesired(FALSE);

		$this->director->setSettings($settings);
		$uuidColumn = $this->director->build($uuidBuilder);

		// Test bracket format --> {XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}
		$this->assertContains("{", $uuidColumn->getData()[0]);
		$this->assertContains("}", $uuidColumn->getData()[0]);
		$this->assertEquals(34, strlen($uuidColumn->getData()[0]));

		$settings->setHyphensDesired(TRUE);
		$settings->setBracketsDesired(TRUE);
		$settings->setUpperCaseDesired(TRUE);

		$this->director->setSettings($settings);
		$uuidColumn = $this->director->build($uuidBuilder);

		// Test hyphen and bracket format --> {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}
		$this->assertContains("{", $uuidColumn->getData()[0]);
		$this->assertContains("}", $uuidColumn->getData()[0]);
		$this->assertEquals(38, strlen($uuidColumn->getData()[0]));
		$this->assertEquals(5, count(explode("-", $uuidColumn->getData()[0])));

		// Test upper case setting
		$this->assertEquals(strtoupper($uuidColumn->getData()[0]), $uuidColumn->getData()[0]);
	}

	public function testCanBuildSingleWordRandomDataFromCsv() {
		$firstNameColumn = $this->director->build(new FirstNameBuilder());

		$this->assertInstanceOf(SingleWordDataColumn::class, $firstNameColumn);
		$this->assertCount(10, $firstNameColumn->getData());
		$this->assertEquals("test", $firstNameColumn->getFieldName());
	}
}

?>