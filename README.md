Data Mock
==================
A data mocking tool that generates random data based on user supplied parameters. A user is allowed to specify the type of each column, the number of rows desired, and the format of the output.

--------------------
## Notes
1. The UI is extremely limited and will remain that way until there is enough functionality to warrant a more robust interface. My initial intention was to generate a codebase that could support the creation of simple data types. With that now completed, I plan on adding more complex data types and see how that impacts usability. That will most likely inform how to properly morph the UI to account for its various use cases.

2. The builder design pattern may appear unnecessary at this stage, but it will simplify scalablity as more complex data types are added.

3. The inspiration came from heavy usage of both [generatedata.com](https://generatedata.com/) and [Mockaroo](https://mockaroo.com/).