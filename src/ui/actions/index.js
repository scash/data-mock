import formUtils from "../utils/formValidation";

export const CREATE_FIELD = "CREATE_FIELD";
export const DELETE_FIELD = "DELETE_FIELD";
export const UPDATE_FIELD_NAME = "UPDATE_FIELD_NAME";
export const UPDATE_FIELD_TYPE = "UPDATE_FIELD_TYPE";
export const UPDATE_FIELD_INDEX = "UPDATE_FIELD_INDEX";

export const ROW_COUNT = "ROW_COUNT";
export const FORM_HAS_ERRORED = "FORM_HAS_ERRORED";

export function submitForm(rcValue, fields) {
    return dispatch => {
        // Set form error state to result of row count validation
        let formError = !formUtils.isRowCountValid(rcValue);

        // Dispatch row count action
        dispatch(rowCount(formError, rcValue));

        // Validate form fields
        let objFieldsValidation = formUtils.traverseFields(fields);

        if(objFieldsValidation.hasErrored) {
            formError = true;
        }

        if(!formError) {
            // fetch
        }

        dispatch(formHasErrored(formError, objFieldsValidation.fields));
    };
}

export function createField(id) {
    return {
        id,
        type: CREATE_FIELD
    };
}

export function deleteField(id) {
    return {
        id,
        type: DELETE_FIELD
    };
}

export function updateFieldName(id, fieldName) {
    return {
        id,
        fieldName,
        type: UPDATE_FIELD_NAME
    };
}

export function updateFieldType(id, fieldType) {
    return {
        id,
        fieldType,
        type: UPDATE_FIELD_TYPE
    };
}

export function updateFieldIndex(oldIndex, newIndex) {
    return {
        oldIndex,
        newIndex,
        type: UPDATE_FIELD_INDEX
    };
}

function formHasErrored(bool, fields) {
    return {
        fields,
        hasErrored: bool,
        type: FORM_HAS_ERRORED
    };
}

function rowCount(bool, value) {
    return {
        value,
        hasErrored: bool,
        type: ROW_COUNT
    };
}