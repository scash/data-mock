import React from "react";
import thunk from "redux-thunk";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import Main from "./components/Main";
import rootReducer from "./reducers";

const store = createStore(rootReducer, applyMiddleware(thunk));

// DELETE ME AT SOME POINT!!!
store.subscribe(() => console.log("store" , store.getState()));
// DELETE ME AT SOME POINT!!!

const app = document.getElementById("main");

ReactDOM.render(
    <Provider store={store}>
        <Main />
    </Provider>, app
);