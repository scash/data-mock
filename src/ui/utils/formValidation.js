export default {
    traverseFields,
    isRowCountValid
}

function traverseFields(fields) {
    let error = false;

    let validationResult = fields.map(field => {
        let fieldNameError = false, fieldTypeError = false;

        if(field.fieldName.trim() === "") {
            error = true;
            fieldNameError = true;
        }

        if(field.fieldType.trim() === "") {
            error = true;
            fieldTypeError = true;
        }

        if(fieldNameError && fieldTypeError) {
            return cloneField(field, "error", "error");
        } else if(fieldNameError) {
            return cloneField(field, "error", null);
        } else if(fieldTypeError) {
            return cloneField(field, null, "error");
        } else {
            return field;
        }
    });

    return {fields: validationResult, hasErrored: error};
}

function isRowCountValid(value) {
    let reg = new RegExp("^\\d+$");

    // Ensure input is numeric
    if(reg.test(value)) {
        let rows = Number(value);

        // Ensure numeric value is positive
        if(rows > 0) {
            return true;
        }
    }

    return false;
}

function cloneField(obj, nameValidationState, typeValidationState) {
    // Deep clone the object
    let clone = JSON.parse(JSON.stringify(obj));

    // Modify clone properties
    clone.errors.fieldNameValidation = nameValidationState;
    clone.errors.fieldTypeValidation = typeValidationState;

    return clone;
}