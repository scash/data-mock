import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Form, FormGroup, FormControl, ControlLabel, Button, Col, Grid, Row } from "react-bootstrap";

import "../css/index.css"
import Fields from "./Fields";
import { submitForm, createField } from "../actions";
import defaultFieldData from "../../data/ui_default_fields.json";

class Main extends React.Component {
    constructor() {
        super();

        // Handle row count ref
        this.rowCountRef = null;
        this.setRowCountRef = element => {
            this.rowCountRef = element;
        };

        this.state = {
            // Basic id increment for new fields
            nextId: (defaultFieldData.length > 0) ? defaultFieldData.length + 1 : 1
        };
    }

    processNewField() {
        this.props.createField(this.state.nextId);

        this.setState({ nextId: this.state.nextId + 1 });
    }

    displayErrorMsg() {
        if(this.props.formHasErrored) {
            return(
                <Row>
                    <Col xs={12} className="form-error-text">
                        Please correct the issues below before submission.
                    </Col>
                </Row>
            );
        }
    }

    render() {
        return(
            <div>
                <Grid fluid>
                    {this.displayErrorMsg()}
                    <Row>
                        <Col xs={1} />
                        <Col xs={3}><b>Field Name</b></Col>
                        <Col xs={3}><b>Field Type</b></Col>
                        <Col xs={4}><b>Options</b></Col>
                        <Col xs={1} />
                    </Row>
                    <Fields />
                    <Row>
                        <Col xs={12}>
                            <Button block onClick={() => this.processNewField()}>Add new field</Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <Form inline>
                                <FormGroup controlId="formRowsDesired"
                                    validationState={this.props.rowCount.validationState}>
                                    <ControlLabel>Rows</ControlLabel>
                                    { " " }
                                    <FormControl type="text" defaultValue={this.props.rowCount.value}
                                        inputRef={this.setRowCountRef} />
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <Button onClick={() => this.props.submitForm(this.rowCountRef.value, this.props.fields)}
                                type="submit">Submit</Button>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        fields: state.fields,
        rowCount: state.rowCount,
        formHasErrored: state.formHasErrored
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        submitForm,
        createField
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);