import React from "react";
import Sortable from "sortablejs"
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FormGroup, FormControl, Col, Row, Glyphicon } from "react-bootstrap";

import {
    deleteField,
    updateFieldName,
    updateFieldType,
    updateFieldIndex
} from "../actions";

class Fields extends React.Component {

    sortableListDecorator(componentBackingInstance) {
        // check if backing instance not null
        if(componentBackingInstance) {
            let options = {
                draggable: ".drag-row", // Specifies which items inside the element should be sortable
                handle: ".drag-handle", // Restricts sort start click/touch to the specified element
                onUpdate: event => { this.props.updateFieldIndex(event.oldIndex, event.newIndex) }
            };

            Sortable.create(componentBackingInstance, options);
        }
    }

    displayFields() {
        return this.props.fields.map(field => {
            return(
                <Row key={field.id} className="drag-row">
                    <Col xs={1}><Glyphicon glyph="menu-hamburger" className="drag-handle" /></Col>
                    <Col xs={3}>
                        <FormGroup controlId="formFieldName" validationState={field.errors.fieldNameValidation}>
                            <FormControl
                                type="text" value={field.fieldName}
                                onChange={event => this.props.updateFieldName(field.id, event.target.value)} />
                        </FormGroup>
                    </Col>
                    <Col xs={3}>
                        <FormGroup controlId="formFieldType" validationState={field.errors.fieldTypeValidation}>
                            <FormControl
                                componentClass="select" value={field.fieldType}
                                onChange={event => this.props.updateFieldType(field.id, event.target.value)}>
                                <option value="">- Select -</option>
                                <option value="fname">First Name</option>
                                <option value="id">Increment ID</option>
                                <option value="ipv4">IPV4</option>
                                <option value="lname">Last Name</option>
                                <option value="uuid">UUID</option>
                            </FormControl>
                        </FormGroup>
                    </Col>
                    <Col xs={4}>
                        <FormGroup controlId="formMisc">
                            <FormControl type="text" placeholder="Misc." />
                        </FormGroup>
                    </Col>
                    <Col xs={1}>
                        <div className="delete-button" onClick={() => this.props.deleteField(field.id)}>
                            <Glyphicon glyph="trash" />
                        </div>
                    </Col>
                </Row>
            );
        });
    }

    render() {
console.log("props ==>", this.props);
        return(
            <div ref={this.sortableListDecorator.bind(this)}>
                {this.displayFields()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        fields: state.fields
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        deleteField,
        updateFieldName,
        updateFieldType,
        updateFieldIndex
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Fields);