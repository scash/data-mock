import { combineReducers } from "redux";

import defaultFieldData from "../../data/ui_default_fields.json";
import {
    CREATE_FIELD,
    DELETE_FIELD,
    UPDATE_FIELD_NAME,
    UPDATE_FIELD_TYPE,
    UPDATE_FIELD_INDEX,

    ROW_COUNT,
    FORM_HAS_ERRORED
} from "../actions";

function fields(state = defaultFieldData, action) {
    switch(action.type) {
        case FORM_HAS_ERRORED:
            return action.fields;
            break;
        case CREATE_FIELD:

            // If applicable, a new field should default to values based on the last entry
            if(state.length > 0) {
                return state.concat([
                    Object.assign({}, state.slice(-1)[0], { id: action.id })
                ]);
            } else {
                return state.concat([{
                    id: action.id,
                    fieldName: "",
                    fieldType: "",
                    errors: {
                        fieldNameValidation: null,
                        fieldTypeValidation: null
                    }
                }]);
            }

            break;
        case DELETE_FIELD:
            return state.filter(field => field.id !== action.id);

            break;
        case UPDATE_FIELD_NAME:
            return state.map(field => {
                if(field.id !== action.id) {
                    return field;
                } else {
                    // Deep clone the field object
                    let clone = JSON.parse(JSON.stringify(field));

                    // Modify clone properties
                    clone.fieldName = action.fieldName;
                    clone.errors.fieldNameValidation = null;

                    return clone;
                }
            });

            break;
        case UPDATE_FIELD_TYPE:
            return state.map(field => {
                if(field.id !== action.id) {
                    return field;
                } else {
                    // Deep clone the field object
                    let clone = JSON.parse(JSON.stringify(field));

                    // Modify clone properties
                    clone.fieldType = action.fieldType;
                    clone.errors.fieldTypeValidation = null;

                    return clone;
                }
            });

            break;
        case UPDATE_FIELD_INDEX:
            let mutable = [...state];
            mutable.splice(action.newIndex, 0, mutable.splice(action.oldIndex, 1)[0]);

            return mutable;

            break;
        default:
            return state;
    }
}

function formHasErrored(state = false, action) {
    switch (action.type) {
        case FORM_HAS_ERRORED:
            return action.hasErrored;
        default:
            return state;
    }
}

function rowCount(state = { value: 10, validationState: null }, action) {
    switch (action.type) {
        case ROW_COUNT:
            return {
                value: action.value,
                // A "validationState" of null equates to no error in FormGroup component
                validationState: action.hasErrored ? "error" : null
            };
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    fields,
    rowCount,
    formHasErrored
});

export default rootReducer;