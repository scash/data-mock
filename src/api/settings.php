<?php

$allSettings = array();				// Aggregated settings
$monologSettings = array();			// Log settings
$bolDisplayErrorDetails = FALSE;	// Level of error information to display

// Ensure log entry is Eastern time zone
//	NOTE: Alternatively, this can be set within php.ini (date.timezone)
date_default_timezone_set("America/New_York");

// Retrieve application environment
//	NOTE: If application environment isn't set, then it's assumed we're running in production
$appMode = ((getenv("APPLICATION_ENV") === FALSE) ? "production" : strtolower(getenv("APPLICATION_ENV")));

switch($appMode) {
	case "development":
		$bolDisplayErrorDetails = TRUE;

		break;
	case "staging":
		break;
	case "production":
}

// Monolog settings
$monologSettings["name"] = "slim-app";
$monologSettings["path"] = "./logs/" . date("Y-m-d") . ".log";

// Aggregate settings
$allSettings["mode"] = $appMode;
$allSettings["logger"] = $monologSettings;
$allSettings["displayErrorDetails"] = $bolDisplayErrorDetails;

?>