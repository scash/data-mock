<?php

$container = $app->getContainer();

// Monolog
$container["logger"] = function ($c) {
	// Obtain Monolog app settings
	$settings = $c->get("settings")["logger"];

	$logger = new Monolog\Logger($settings["name"]);

	// Monolog processor that logs server information as part of each entry
	$logger->pushProcessor(new Monolog\Processor\WebProcessor($_SERVER));

	// Monolog handler that writes to a log file
	$logger->pushHandler(new Monolog\Handler\StreamHandler($settings["path"], Monolog\Logger::DEBUG));

	return $logger;
};

?>