<?php

namespace Utilities;

/**
 * Utility methods
 *
 * @author Santos Cash
 */
class Util {

	/**
	 * Determine if the value of a variable represents an integer
	 *
	 * @param	mixed	$input	Input variable
	 * @return	bool	Whether the passed value can be cast to an int
	 */
	public static function isInteger($input) {
		return(ctype_digit(strval($input)));
	}

	/**
	 * Log and alert client to any errors encountered during application execution
	 *
	 * @param	Monolog\Logger	$logger	Object used to log application messages
	 * @param	string	$errorMsg	Error message
	 * @param	int	$statusCode	HTTP status code to send to the client
	 * @param	string	$specialCode	A code indicating specific application actions
	 * @return	Slim\Http\Response	A JSON response containing error information
	 */
	public static function handleError($logger, $errorMsg, $statusCode = 403, $specialCode = NULL) {

		// Log error
		$logger->error("Status: " . $statusCode . " -- " . $errorMsg);

		if(!is_null($specialCode)) {
			$error = new stdClass();
			$error->code = $specialCode;
			$error->message = $errorMsg;
		} else {
			$error = $errorMsg;
		}

		// Craft client response
		$response = new SlimResponse(array("error" => $error));
		$response->setStatusCode($statusCode);
		$response->setHeader("X-Status-Reason", $errorMsg);

		return $response->getResponse();
	}

}

?>