<?php

namespace Utilities;

/**
 * Handle an HTTP response within the Slim framework
 *
 * @author Santos Cash
 */
class SlimResponse {

	/**
	 * The body of the response.
	 *
	 * @var	string
	 */
	private $body;

	/**
	 * The HTTP status code of the Response.
	 *
	 * @var	int
	 */
	private $statusCode;

	/**
	 * Encode the response as JSON?
	 *
	 * @var	bool
	 */
	private $jsonEncode;

	/**
	 * For a JSON response, should numeric strings be converted to integers?
	 *
	 * @var	bool
	 */
	private $jsonNumeric;

	/**
	 * Headers to apply to the response.
	 *
	 * @var	array
	 */
	private $headerVars;

	/**
	 * Constructor that sets all necessary default settings.
	 *
	 * @param	string	$body	The body of the response
	 */
	public function __construct($body = NULL) {
		$this->body = $body;

		// Set default values
		$this->statusCode = 200;
		$this->jsonEncode = TRUE;
		$this->jsonNumeric = FALSE;
		$this->setHeader("Content-Type", "application/json");
	}

	public function setStatusCode($statusCode) {
		$this->statusCode = $statusCode;
	}

	public function setJsonEncode($jsonEncode) {
		$this->jsonEncode = $jsonEncode;
	}

	public function setJsonNumeric($jsonNumeric) {
		$this->jsonNumeric = $jsonNumeric;
	}

	/**
	 * Set a response header and its corresponding value.
	 *
	 * @param	string	$name	Header name
	 * @param	string|string[]	$value	Header value(s)
	 */
	public function setHeader($name, $value) {
		$this->headerVars[$name] = $value;
	}

	/**
	 * Send the response to the client.
	 *
	 * @return	Slim\Http\Response	A response to send to the client
	 */
	public function getResponse() {
		$headers = new \Slim\Http\Headers($this->headerVars);
		$response = new \Slim\Http\Response($this->statusCode, $headers);

		// HTTP status code of 204 should never return a response body
		if($this->statusCode === 204) {
			return $response;
		} else {
			$responseBody = $this->body;

			// Encode response body as JSON
			if($this->jsonEncode && !is_null($responseBody)) {
				$responseBody = ($this->jsonNumeric ? json_encode($responseBody, JSON_NUMERIC_CHECK) : json_encode($responseBody));
			}

			$returnBodyStream = $response->getBody();
			$returnBodyStream->write($responseBody);

			return $response->withBody($returnBodyStream);
		}
	}

}

?>