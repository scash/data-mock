<?php

session_start();

require_once("settings.php");
require_once("../../vendor/autoload.php");

// Instantiate the application
$settings = array("settings" => $allSettings);
$app = new \Slim\App($settings);

// Handle dependencies
require_once("dic.php");

// Register routes
include_once("routes.php");

$app->run();

?>