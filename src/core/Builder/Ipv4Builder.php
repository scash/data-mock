<?php

namespace DataMock\Builder;

use DataMock\Column\DataColumn;
use DataMock\Column\Ipv4DataColumn;

use DataMock\Settings\BaseSettings;

/**
 * Builder service to create random IPV4 values
 *
 * @author Santos Cash
 */
class Ipv4Builder implements DataBuilderInterface {

	/**
	 * The data column.
	 *
	 * @var	Ipv4DataColumn
	 */
	private $column;

	public function createDataColumn(BaseSettings $settings): void {
		$this->column = new Ipv4DataColumn();

		$this->column->setRowCount($settings->getRowCount());
		$this->column->setFieldName($settings->getFieldName());
	}

	public function generateRandomData(): void {
		$this->column->generateRandomData();
	}

	public function getDataColumn(): DataColumn {
		return $this->column;
	}
}

?>