<?php

namespace DataMock\Builder;

use DataMock\Column\DataColumn;

use DataMock\Settings\BaseSettings;

/**
 * The functionality that all builder services must implement,
 * 	in accordance with the builder design pattern
 *
 * @author Santos Cash
 */
interface DataBuilderInterface {
	public function createDataColumn(BaseSettings $settings): void;
	public function generateRandomData(): void;
	public function getDataColumn(): DataColumn;
}

?>