<?php

namespace DataMock\Builder;

use DataMock\Column\DataColumn;
use DataMock\Column\IdDataColumn;

use DataMock\Settings\BaseSettings;

/**
 * Builder service to create incremental numeric values
 *
 * @author Santos Cash
 */
class IdBuilder implements DataBuilderInterface {

	/**
	 * The data column.
	 *
	 * @var	IdDataColumn
	 */
	private $column;

	/**
	 * The ID settings.
	 *
	 * @var	BaseSettings
	 */
	private $settings;

	public function createDataColumn(BaseSettings $settings): void {
		$this->settings = $settings;

		$this->column = new IdDataColumn();
		$this->column->setRowCount($settings->getRowCount());
		$this->column->setFieldName($settings->getFieldName());
	}

	public function generateRandomData(): void {
		$this->column->generateRandomData($this->settings);
	}

	public function getDataColumn(): DataColumn {
		return $this->column;
	}
}

?>