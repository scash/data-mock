<?php

namespace DataMock\Builder;

use DataMock\Column\DataColumn;
use DataMock\Column\UuidDataColumn;

use DataMock\Settings\BaseSettings;

/**
 * Builder service to create random UUID values
 *
 * @author Santos Cash
 */
class UuidBuilder implements DataBuilderInterface {

	/**
	 * The data column.
	 *
	 * @var	UuidDataColumn
	 */
	private $column;

	/**
	 * The UUID settings.
	 *
	 * @var	BaseSettings
	 */
	private $settings;

	public function createDataColumn(BaseSettings $settings): void {
		$this->settings = $settings;

		$this->column = new UuidDataColumn();
		$this->column->setRowCount($settings->getRowCount());
		$this->column->setFieldName($settings->getFieldName());
	}

	public function generateRandomData(): void {
		$this->column->generateRandomData($this->settings);
	}

	public function getDataColumn(): DataColumn {
		return $this->column;
	}
}

?>