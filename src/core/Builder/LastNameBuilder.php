<?php

namespace DataMock\Builder;

use DataMock\Column\DataColumn;
use DataMock\Column\SingleWordDataColumn;

use DataMock\Helper\FileReader;

use DataMock\Settings\BaseSettings;

/**
 * Builder service to create random last names
 *
 * @author Santos Cash
 */
class LastNameBuilder implements DataBuilderInterface {

	/**
	 * The data column.
	 *
	 * @var	SingleWordDataColumn
	 */
	private $column;

	/**
	 * The file.
	 *
	 * @var	\SplFileObject
	 */
	private $fileReader;

	public function createDataColumn(BaseSettings $settings): void {
		try {
			$filePath = realpath(dirname(__FILE__) . "/../../data/last_names.csv");

			$this->column = new SingleWordDataColumn();
			$this->fileReader = new FileReader($filePath);

			$this->column->setRowCount($settings->getRowCount());
			$this->column->setFieldName($settings->getFieldName());
			$this->column->setFileReader($this->fileReader);
		} catch(\Exception $e) {
			// Todo: Log error and inform user of internal issue (file not found)
		}
	}

	public function generateRandomData(): void {
		$this->column->generateRandomData();
		$this->fileReader->deleteFileInMemory();
	}

	public function getDataColumn(): DataColumn {
		return $this->column;
	}
}

?>