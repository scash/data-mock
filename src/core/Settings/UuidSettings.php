<?php

namespace DataMock\Settings;

/**
 * Settings of UUID columns
 *
 * @author Santos Cash
 */
class UuidSettings extends BaseSettings {

	/**
	 * Should the end result have hyphens?
	 *
	 * @var	bool
	 */
	private $hyphens;

	/**
	 * Should the end result have brackets?
	 *
	 * @var	bool
	 */
	private $brackets;

	/**
	 * Should the end result be in upper case?
	 *
	 * @var	bool
	 */
	private $upperCase;

	/**
	 * Constructor that defaults to a lower case UUID with no hyphens or brackets.
	 */
	public function __construct(bool $hyphens = FALSE, bool $brackets = FALSE, bool $upperCase = FALSE) {
		$this->hyphens = $hyphens;
		$this->brackets = $brackets;
		$this->upperCase = $upperCase;
	}

	public function setHyphensDesired(bool $hyphens): void {
		$this->hyphens = $hyphens;
	}

	public function isHyphensDesired(): bool {
		return $this->hyphens;
	}

	public function setBracketsDesired(bool $brackets): void {
		$this->brackets = $brackets;
	}

	public function isBracketsDesired(): bool {
		return $this->brackets;
	}

	public function setUpperCaseDesired(bool $upperCase): void {
		$this->upperCase = $upperCase;
	}

	public function isUpperCaseDesired(): bool {
		return $this->upperCase;
	}
}

?>