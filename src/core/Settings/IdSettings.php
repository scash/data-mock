<?php

namespace DataMock\Settings;

/**
 * Settings of ID columns
 *
 * @author Santos Cash
 */
class IdSettings extends BaseSettings {

	/**
	 * Starting numeric value from which to increment from
	 *
	 * @var	int
	 */
	private $number;

	/**
	 * Constructor that defaults to a starting value of one.
	 */
	public function __construct(int $startVal = 1) {
		$this->number = $startVal;
	}

	public function setStartValue(int $startVal): void {
		$this->number = $startVal;
	}

	public function getStartValue(): int {
		return $this->number;
	}

}

?>