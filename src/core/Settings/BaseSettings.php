<?php

namespace DataMock\Settings;

/**
 * The base settings of all columns
 *
 * @author Santos Cash
 */
class BaseSettings {

	/**
	 * The number of rows desired.
	 *
	 * @var	int
	 */
	protected $rows;

	/**
	 * The column name.
	 *
	 * @var	string
	 */
	protected $fieldName;

	public function setRowCount(int $rows): void {
		$this->rows = $rows;
	}

	public function getRowCount(): int {
		return $this->rows;
	}

	public function setFieldName(string $fieldName): void {
		$this->fieldName = $fieldName;
	}

	public function getFieldName(): string {
		return $this->fieldName;
	}
}

?>