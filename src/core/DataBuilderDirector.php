<?php

namespace DataMock;

use DataMock\Builder\DataBuilderInterface;

use DataMock\Column\DataColumn;

use DataMock\Settings\BaseSettings;

/**
 * The director that invokes builder services,
 * 	in accordance with the builder design pattern
 *
 * @author Santos Cash
 */
class DataBuilderDirector {

	/**
	 * The column settings.
	 *
	 * @var	BaseSettings
	 */
	private $settings;

	/**
	 * Constructor that sets all necessary column settings.
	 *
	 * @param	BaseSettings	$settings	The column settings
	 */
	public function __construct(BaseSettings $settings) {
		$this->settings = $settings;
	}

	/**
	 * Construction process leveraged by all invoked builders.
	 *
	 * @param	DataBuilderInterface	$builder	Builder to invoke
	 * @return	DataColumn	A DataColumn representation of the invoked builder
	 */
	public function build(DataBuilderInterface $builder): DataColumn {
		$builder->createDataColumn($this->settings);
		$builder->generateRandomData();

		return $builder->getDataColumn();
	}

	public function setSettings(BaseSettings $settings) {
		$this->settings = $settings;
	}
}

?>