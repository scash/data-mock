<?php

namespace DataMock\Column;

/**
 * A data column representing random string values obtained from a CSV file
 *
 * @author Santos Cash
 */
class SingleWordDataColumn extends DataColumn {

	/**
	 * The file.
	 *
	 * @var	\SplFileObject
	 */
	private $fileReader;

	public function generateRandomData(): void {
		$fileData = $this->fileReader->parseFileAsCsv();

		if(!empty($fileData)) {
			for($i = 0; $this->rows > $i; $i++) {
				$this->setSingleDataValue($fileData[mt_rand(0, count($fileData) - 1)][0]);
			}
		} else {
			// Todo: Log error and inform user of internal issue (file is blank)
		}
	}

	public function setFileReader($fileReader) {
		$this->fileReader = $fileReader;
	}
}

?>