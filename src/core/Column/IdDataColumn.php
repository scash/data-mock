<?php

namespace DataMock\Column;

use DataMock\Settings\IdSettings;

/**
 * A data column representing increasing numeric values
 *
 * @author Santos Cash
 */
class IdDataColumn extends DataColumn {

	/**
	 * Generate random ID values that adhere to specified settings.
	 *
	 * @param	IdSettings	$settings	The ID settings
	 */
	public function generateRandomData(IdSettings $settings): void {
		$intStartVal = $settings->getStartValue();

		for($i = $intStartVal; ($this->rows + $intStartVal) > $i; $i++) {
			$this->setSingleDataValue($i);
		}
	}

}

?>