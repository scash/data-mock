<?php

namespace DataMock\Column;

use DataMock\Settings\UuidSettings;

/**
 * A data column representing random UUID values
 *
 * @author Santos Cash
 */
class UuidDataColumn extends DataColumn {

	/**
	 * Generate random UUID values that adhere to specified format settings.
	 *
	 * @param	UuidSettings	$settings	The UUID format settings
	 */
	public function generateRandomData(UuidSettings $settings): void {
		$format = "";
		$formattedData = array();

		if($settings->isBracketsDesired() && $settings->isHyphensDesired()) {
			$format = "{%s%s-%s-%s-%s-%s%s%s}";
		} elseif($settings->isBracketsDesired()) {
			$format = "{%s%s%s%s%s%s%s%s}";
		} elseif($settings->isHyphensDesired()) {
			$format = "%s%s-%s-%s-%s-%s%s%s";
		} else {
			$format = "%s%s%s%s%s%s%s%s";
		}

		for($i = 0; $this->rows > $i; $i++) {
			// Hexadecimal representation of 16 random bytes of data
			$hex = bin2hex(openssl_random_pseudo_bytes(16));

			$formattedData[] = vsprintf($format, str_split($hex, 4));
		}

		if($settings->isUpperCaseDesired()) {
			$this->setData(array_map("strtoupper", $formattedData));
		} else {
			$this->setData($formattedData);
		}
	}

}

?>