<?php

namespace DataMock\Column;

/**
 * The base class for all data column objects
 *
 * @author Santos Cash
 */
abstract class DataColumn {

	/**
	 * The number of rows desired.
	 *
	 * @var	int
	 */
	protected $rows;

	/**
	 * The column name.
	 *
	 * @var	string
	 */
	protected $fieldName;

	/**
	 * The column data for each row.
	 *
	 * @var	array
	 */
	protected $data = array();

	public function setRowCount(int $rows): void {
		$this->rows = $rows;
	}

	public function getRowCount(): int {
		return $this->rows;
	}

	public function setFieldName(string $fieldName): void {
		$this->fieldName = $fieldName;
	}

	public function getFieldName(): string {
		return $this->fieldName;
	}

	public function setData(array $data): void {
		$this->data = $data;
	}

	public function getData(): array {
		return $this->data;
	}

	public function setSingleDataValue($val): void {
		$this->data[] = $val;
	}
}

?>