<?php

namespace DataMock\Column;

/**
 * A data column representing random IPV4 values
 *
 * @author Santos Cash
 */
class Ipv4DataColumn extends DataColumn {

	public function generateRandomData(): void {
		for($i = 0; $this->rows > $i; $i++) {	
			$this->setSingleDataValue(sprintf("%d.%d.%d.%d", mt_rand(0,255), mt_rand(0,255), mt_rand(0,255), mt_rand(0,255)));
		}
	}

}

?>