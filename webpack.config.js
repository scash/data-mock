const path = require("path");
const webpack = require("webpack");

module.exports = {
    context: path.resolve(__dirname, "src/ui"),
    entry: ["./index.html", "./index.js"],
    output: {
        path: path.resolve(__dirname, "public"),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.resolve(__dirname, "src/ui"),    // Code to be transformed
                loader: "babel-loader",
                options: {
                    presets: ["env", "react"]
                }
            },
            {
                test: /\.(png|jpg|gif|html)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[path][name].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [ "style-loader", "css-loader" ]
            }
        ]
    },
    //
    //  Dev environment specific, use webpack-merge to split once ready for prod
    //
    devtool: "eval-source-map",
    devServer: {
        contentBase: path.resolve(__dirname, "public"),
        compress: true,
        index: "index.html",
        port: 3000
    }
};